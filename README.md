## Lection 1: Getting started

Android has been taking the world by storm. Everybody wants a smart phone or tablet, and Android devices are hugely popular. Along the way you’ll meet some of the basic components of all Android apps such as activities and layouts. **All you need is a little Java know-how;**

![img](http://dogriffiths.github.io/HeadFirstAndroid/images/chap01img.png)

## Lection 2: Layouts & View

![img](http://dogriffiths.github.io/HeadFirstAndroid/images/chap02img.png)

## Lection 3: Multiple activities and intents. The activity lifecycle

![img](http://dogriffiths.github.io/HeadFirstAndroid/images/chap03img.png)

## Lection 4: List views and adapters

Want to know how to best structure your Android app? You’ve learned some of the basic pieces that are user to build apps, and now *it’s time to get organized*. In this Lection we’ll show you how you can take a bunch of ideas and ***structure them to build an awesome app***. We’ll show you how **lists of data** can form the core part of your app design, and how **linking them together** can create a *powerful and easy-to-use app*. Along the way, you get your first glimpse of using **event listeners** and **adapters** to make your app more dynamic.

![img](http://dogriffiths.github.io/HeadFirstAndroid/images/chap06img.png)

## Lection 5: SQLite databases

If you’re recording high scores or saving tweets, your app will need to store data. And on Android you usually keep your data safe inside a **SQLite database**. In this lection, we’ll show you how to *create a database, add tables to it*, and *prepopulate it with data*, all with the help of the friendly **SQLite helper**. You’ll then see how you can cleanly roll out *upgrades* to your database structure, and how to *downgrade* it if you need pull any changes.

![img](http://dogriffiths.github.io/HeadFirstAndroid/images/chap07img.png)

## Lection 6: Cursors and AsyncTasks

So how do you connect your app to a SQLite database? So far you’ve seen how to create a SQLite database using a SQLite helper. The next step is to get your activities to access it. In this lection you’ll find out how to use **cursors** to get data from the database, how to *navigate* through cursors and how to *get data from them*. You’ll then find out how to use **cursor adapters** to connect them to list views. Finally, you’ll see how writing efficient *multi-threaded* code with **AsyncTasks** will keep your app speedy.

![img](http://dogriffiths.github.io/HeadFirstAndroid/images/chap08img.png)

## Lection 7: Services

There are some operations you want to keep on running irrespective of which app has the focus. As an example, If you start playing a music file in a music app, you probably expect it to keep on playing when you switch to another app. In this lection you’ll see how to use **Services** to deal with situations just like this. Along the way you’ll see how use some of **Android’s built-in services**. You’ll see how to to keep your users informed with the *notification service*, and how the *location service* can tell you where you’re located.

![img](http://dogriffiths.github.io/HeadFirstAndroid/images/chap09img.png)

## Lection 8: Fragments

You’ve seen how to create apps that work in the same way irrespective of the device they’re running on. But what if you want your app to look and *behave differently* depending on whether it’s running on a *phone* or a *tablet*? In this lection we’ll show you how to make your app choose the **most appropriate layout for the device screen size**. We’ll also introduce you to **fragments**, a way of creating *modular code components* that can be *reused by different activities*.

![img](http://dogriffiths.github.io/HeadFirstAndroid/images/chap11img.png)

## Lection 9: Action Bars & Navigation Drawers

Everybody likes a shortcut. And in this lection you’ll see how to add shortcuts to your apps using **action bars**. We’ll show you how to start other activities by **adding action items** to your action bar, how to share content with other apps using the **share action provider**, and how to navigate up your app’s hierarchy by implementing **the action bar’s Up button**. Along the way you’ll see how to give your app a consistant look and feel using **themes**, and introduce you to the Android support library package.

![img](http://dogriffiths.github.io/HeadFirstAndroid/images/chap12img.png)

Apps are so much better when they’re easy to navigate. In this lection we’re going to introduce you to the **navigation drawer**, a slide-out panel that appears when you swipe your finger or click an icon on the action bar. We’ll show you how to use it to display a *list of links* that take you to **all the major hubs** of your app. You’ll also see how *switching fragments* makes those hubs **easy to get to** and **fast to display**.

![img](http://dogriffiths.github.io/HeadFirstAndroid/images/chap13img.png)

Lection 10: Material Design

With API level 21, Google introduced Material Design. In this lection we’ll look at **what Material Design is**, and how to make your apps fit in with it. We’ll start by introducing you to **card views** you can reuse across your app for a *consistent look and feel*. Then we’ll introduce you to the **recycler view**, the list view’s flexible friend. Along the way you’ll see how to **create your own adapters**, and how to completely change the look of a recycler view with *just two lines of code*.

![img](http://dogriffiths.github.io/HeadFirstAndroid/images/chap14img.png)